// Stored query ids.
             var STORED_QUERY_OBSERVATION = "fmi::observations::weather::multipointcoverage";
            //var STORED_QUERY_OBSERVATION = "fmi::observations::mareograph::timevaluepair";

            // URL for test server.
            var TEST_SERVER_URL = "http://data.fmi.fi/fmi-apikey/209025cc-a8d3-4e03-a5b5-0711dc1c204f/wfs";
            if (TEST_SERVER_URL.indexOf("insert-your-apikey-here") !== -1) {
                alert("Check connection.html source! TEST_SERVER_URL should contain proper API-key!");
            }

            /**
             * This function recursively browses the given {data} structure and appends the content as text
             * to the {container} element.
             *
             * @param {Element} container Content is appended as a text here.
             * @param {Object|Array|String|etc} data Content that is browsed through recursively.
             * @param {String} indentStr Indentation string of the previous recursion level.
             */

            function recursiveBrowse(container, data, indentStr) {
                if (_.isArray(data) || _.isObject(data)) {
                    // Browse all the child items of the array or object.
                    indentStr += ">";
                    _.each(data, function(value, key) {
                        container.append("<br>" + indentStr + " [" + key + "]");
                        recursiveBrowse(container, value, indentStr);
                    });

                } else {
                    // This is a leaf. So, just append it after its container array or object.
                    container.append(" = " + data);
                }
            }

            /**
             * Handle parser results in this callback function.
             *
             * Append result strings to the UI.
             *
             * @param {Object} data Parsed data.
             * @param {Object} errors Parser errors.
             * @param {String} test case name.
             */
            function handleCallback(data, errors, caseName) {
                var results = jQuery("#results");
                results.append("<h2>" + caseName + "</h2>");
                if (data) {
                    results.append("<h3>Data object</h3>");
                    recursiveBrowse(results, data, "#");
                }
                if (errors) {
                    results.append("<h3>Errors object</h3>");
                    recursiveBrowse(results, errors, "#");
                }
            }

            function getData(url) {
                // See API documentation and comments from connection source code of
                // fi.fmi.metoclient.metolib.WfsConnection.getData function for the description
                // of function options parameter object and for the callback parameters objects structures.
                var connection = new fi.fmi.metoclient.metolib.WfsConnection();

                var from = new Date($("#fieldStartDate").val()).getTime();
                var to =new Date($("#fieldEndDate").val()).getTime();
                console.log(from);
                console.log(to);


                if (connection.connect(url, STORED_QUERY_OBSERVATION)) {
                    // Connection was properly initialized. So, get the data.
                    connection.getData({
                        requestParameter : "td",
                        // Integer values are used to init dates for older browsers.
                        // (new Date("2013-05-10T08:00:00Z")).getTime()
                        // (new Date("2013-05-12T10:00:00Z")).getTime()
                        begin : new Date(from),
                        end : new Date(to),
                        timestep : 60 * 60 * 1000,
                        sites : $("#fieldLocation").val(),
                        callback : function(data, errors) {
                            // Handle the data and errors object in a way you choose.
                            // Here, we delegate the content for a separate handler function.
                            // See parser documentation from source code comments for more details.


                            handleCallback(data, errors, $("#fieldLocation").val());
                            //$("#results").html(data);
                            // Disconnect because the flow has finished.
                            connection.disconnect();
                        }
                    });
                }
                else
                {
                  alert("Connection was not properly initialized");
                }
            }




            $(function() {
                // Run test cases by using functions above.
                $("#buttBegin").click(function() {
                   getData(TEST_SERVER_URL);
                } );


            });