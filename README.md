# README #

We describe a new convenient tool for programmatically mining and parsing XML data fed by the FMI Open Data API. It is packaged as a Python program with no external dependencies, and runs on Python 3.3 and higher.
In its current stage, it provides an easy command-line based interface to mine datasets in TimeValuePair format4. The tool provides easy output of necessary data into a file that is designed as being ready to be parsed into other analysis suites like MATLAB.

The tool provides configurable parameters to allow the user to set:

*	The location (through coordinate pair or via the unique identifier (FMISID) of the meteorological station);
*	A time range and resolution of the retrieved data set;
*	The format of the output file;
*	The time-series to extract.

Omission of any of the listed parameters results in fetching all the available data for the omitted parameter. For example, omitting the time-series parameter will result in trying to extract all the available time-series at the specified location.
The tool also provides a workaround for a time-length limitation of the FMI API. The current API only is able to provide data for up to 168 consecutive hours. The new tool allows for retrieval of datasets of any desired length.
The output of the tool is a set of files described as follows:

*	A text file containing human readable meta information regarding the extracted data;
*	A text file for each selected time-series, formatted according to the specified configuration.