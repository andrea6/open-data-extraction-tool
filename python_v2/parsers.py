from namespaces import NAMESPACES
import xml.etree.ElementTree as ET
import urllib.request
from datetime import datetime
import re

class FMIDataParser(object):

    def __init__(self, apikey):
        self.timeseries = []
        self.apikey = apikey
        self.query = "http://data.fmi.fi/fmi-apikey/%s/wfs?request=getFeature" % (self.apikey)
        self.dateformat = "%Y-%m-%dT%H:%M:%SZ"

    def parse():
        pass

    def getTimeSeries():
        pass




# A TimeValuePairParser is an FMIDataParser which specifies  its own method to parse the retrieved data.
class TimeValuePairParser(FMIDataParser):

    def __init__(self, apikey, fmisid = None, starttime = None, endtime = None):
       super().__init__(apikey)
       if fmisid is not None:
           self.query += "&fmisid=%s" % (fmisid)
       if starttime is not None:
           self.query += "&starttime=%s" % (starttime)
       if endtime is not None:
           self.query += "&endtime=%s" % (endtime)

       if starttime is not None and endtime is not None:
           delta = datetime.strptime(endtime, self.dateformat)- datetime.strptime(starttime, self.dateformat)
           tothours = delta.days *24 + delta.seconds // 3600
           print(tothours)
           if tothours > 160:
               raise Exception("\n######  Open Data allowed only for 160 hours maximum  length time ranges\n")
            
    # Parses the XML content of the response from FMI Open Data server
    def parse(self):
        xml = urllib.request.urlopen(self.query).read()
        tree = ET.fromstring(xml)
        # retrieves all the <omg:PointTimeSeriesObservation>
        # get wfs:members
        tsobservations = []


        # UGLY FIX 23/4/2015
        members = tree.getchildren()
        for member in members:
            tsobservations.append(member.getchildren()[0])
        # tsobservations = tree.findall('.//omso:PointTimeSeriesObservation', NAMESPACES)
        # / UGLYFIX
        starttime = None
        endtime = None
        geoid = None
        locationname = None
        observationname = None
        for obs in tsobservations:
            #<gml:beginPosition> is Start Time
            # Both start and end are often explicit in the first timeserie, and
            # are just referenced with internal hrefs by the following series.
            # For some reason, ElementTree doesn't resolve those links so we do it
            # manually.
            bp = obs.find('.//gml:beginPosition', NAMESPACES)
            if bp is not None:
                starttime = datetime.strptime(bp.text, self.dateformat)
            #<gml:endPosition> is End Time
            ep = obs.find('.//gml:endPosition', NAMESPACES)
            if ep is not None:
                endtime = datetime.strptime(ep.text, self.dateformat)
            # <om:featureofInterest>
            feature = obs.find('.//om:featureOfInterest', NAMESPACES)
            if feature is not None:
                #<target:Location>
                location = feature.find('.//target:Location', NAMESPACES)
                #<gml:identifier>
                fmisid_tag = feature.find('.//gml:identifier', NAMESPACES)
                if fmisid_tag is not None:
                    fmisid = fmisid_tag.text
                # <gml:name>
                names = location.findall('.//gml:name', NAMESPACES)
                for name in names:
                    cs = name.get('codeSpace', None)
                    if cs == "http://xml.fmi.fi/namespace/locationcode/geoid":
                       geoid = name.text
                    if cs == "http://xml.fmi.fi/namespace/locationcode/name":
                       locationname = name.text
                observationname = feature.find('.//sams:SF_SpatialSamplingFeature', NAMESPACES).get('{http://www.opengis.net/gml/3.2}id', None)
            result = obs.find('.//om:result', NAMESPACES)
            times = result.findall('.//wml2:time', NAMESPACES)
            values = result.findall('.//wml2:value', NAMESPACES)
            # zip together time and value pairs into a list of TimeStampedValues
            list_tsv = [ TimeStampedValue(datetime.strptime(el[0].text, self.dateformat), el[1].text) for el in zip(times,values) ]

            member = TimeValueDataSerie()
            member.starttime = starttime
            member.endtime = endtime
            member.geoid = geoid
            member.fmisid = fmisid
            member.locationname = locationname
            member.observationname = observationname

            member.data = list_tsv
            self.timeseries.append(member)


    def getTimeSeries(self):
        return self.timeseries

    def save(self, timeseries=None):
        for ts in self.timeseries:
            found = False
            if timeseries is not None:
                for n in timeseries:
                    found = found or n.lower() in ts.observationname.lower()
                if not found:
                    continue
                

            base_filename = str(ts.fmisid)+"_FROM_"+ts.starttime.strftime('%m-%d-%Y')+"_TO_"+ts.endtime.strftime('%m-%d-%Y')+"."+ts.observationname
                
            with open(base_filename , 'w') as t:
                t.write("Location name\t"+ts.locationname+"\n")
                t.write("Observation\t"+ts.observationname+"\n")
                t.write("FMISID\t"+ts.fmisid+"\n")
                t.write("Start Time\t"+str(ts.starttime)+"\n")
                t.write("End Time\t"+str(ts.endtime)+"\n")
                t.write("### Begin Time Series ###\n")
                for sample in ts.data:
                    t.write(str(sample.getTimestamp())+"\t"+sample.getValue()+"\n")

    def __str__(self):
        ret = "TimeValuePairParser"
        if len(self.timeseries) > 0:
            ret += "\n\t Available data series:"
            for ts in self.timeseries:
                ret += "\n\t" + ts.observationname + "\t" + ts.locationname + "\t\t\t(" + str(len(ts.data))+") samples" 
        else:
            ret += "\n No data available"

        return ret
        





class MareographObservation(TimeValuePairParser):
    def __init__(self, apikey, fmisid = None, starttime = None, endtime = None):
        super().__init__(apikey, fmisid, starttime, endtime)
        self.query += "&storedquery_id=fmi::observations::mareograph::timevaluepair"

class WaveObservation(TimeValuePairParser):
    def __init__(self, apikey, fmisid = None, starttime = None, endtime = None):
        super().__init__(apikey, fmisid, starttime, endtime)
        self.query += "&storedquery_id=fmi::observations::wave::timevaluepair"

class WeatherObservation(TimeValuePairParser):
    def __init__(self, apikey, fmisid = None, starttime = None, endtime = None):
        super().__init__(apikey, fmisid, starttime, endtime)
        self.query += "&storedquery_id=fmi::observations::weather::timevaluepair"

class TimeValueDataSerie(object):
    def __init__(self):
        self.starttime = None
        self.endtime = None
        self.fmisid = None
        self.geoid = None
        self.locationname = None
        self.observationname = None
        self.data = []



class TimeStampedValue(object):
    def __init__(self, timestamp, value):
        self.__timestamp = timestamp
        self.__value = value

    def getTimestamp(self):
        return self.__timestamp

    def getValue(self):
        return self.__value
            



