NAMESPACES = { 'wfs' : 'http://www.opengis.net/wfs/2.0',
        'om' : 'http://www.opengis.net/om/2.0',
        'ompr' : 'http://inspire.ec.europa.eu/schemas/ompr/2.0',
        'gmlcov' : 'http://www.opengis.net/gmlcov/1.0',
        'gml' : 'http://www.opengis.net/gml/3.2',
        'gmd' : 'http://www.isotc211.org/2005/gmd',
        'gco' : 'http://www.isotc211.org/2005/gco',
        'wml2' : 'http://www.opengis.net/waterml/2.0',
        'omso' : 'http://inspire.ec.europa.eu/schemas/omso/2.0/SpecialisedObservations.xsd',
        'xlink' : 'http://www.w3.org/1999/xlink',
        'xsi' : 'http://www.w3.org/2001/XMLSchema-instance',
        'target' : 'http://xml.fmi.fi/namespace/om/atmosphericfeatures/1.0',
        'sams' : 'http://www.opengis.net/samplingSpatial/2.0',
        'sam' : 'http://www.opengis.net/sampling/2.0'
        
        }
