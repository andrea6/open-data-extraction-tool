import xml.etree.ElementTree as ET
from parsers import MareographObservation, WaveObservation, WeatherObservation
import argparse




def main():
    print("FMI Open Data Downloader v0.7")
    args = parse_command_line()

    parser = None
    
    if args.data == 'wave':
        parser = WaveObservation(args.api, fmisid=args.location, starttime=args.start, endtime=args.end)
    if args.data == 'mareograph':
        parser = MareographObservation(args.api, fmisid=args.location, starttime=args.start, endtime=args.end)
    if args.data == 'weather':
        parser = WeatherObservation(args.api, fmisid=args.location, starttime=args.start, endtime=args.end)
    print(parser.query)
    parser.parse()
    print(parser)
    
    print(args.timeseries)

    if args.silent or  input('Do you want to save the data as text files (y/n)? :') == 'y':
        parser.save(timeseries=args.timeseries)



def parse_command_line():
    parser = argparse.ArgumentParser(description="Retrieves and parses FMI Open Data sets in TimeValuePair format into scientist-friendly formats.")
    parser.add_argument('api', help="The API key provided by the FMI is required. You can obtain one for free from the FMI website")
    parser.add_argument('data', help="The data type to retrieve.", choices = ["wave", "mareograph", "weather"])
    parser.add_argument('-t', '--timeseries',  nargs='+', type=str, help="Specifies a subset of time series types to retrieve. Example: -t wavehs watlev modalwdi whdd")
    parser.add_argument('--silent', help="Use this flag to prevent the program from asking user input.", action='store_true' )
    parser.add_argument('-l', '--location', help="FMISID of the location to retrieve data from" )
    parser.add_argument('-s', '--start', help="Start date with format 'yyyy-mm-ddThh:mm:ssZ'")
    parser.add_argument('-e', '--end', help="End date with format 'yyyy-mm--ddThh:mm:ssZ'")
    args = parser.parse_args()
    return args


# entry point
if __name__ == "__main__":
    main()
