Get capabilities:

http://data.fmi.fi/fmi-apikey/209025cc-a8d3-4e03-a5b5-0711dc1c204f/wfs?request=GetCapabilities


Desribe Stored Query command:

http://data.fmi.fi/fmi-apikey/209025cc-a8d3-4e03-a5b5-0711dc1c204f/wfs?request=DescribeStoredQueries&storedquery_id=fmi::observations::mareograph::timevaluepair



Location parameters in stored queries
bbox -> area
        By default, in same srs with the (requested) srs. Specify as fifth element if needed (i.e. &bbox=132249,6433579,631999,6933329,epsg:3067)
fmisid -> FMI id for observation station
wmo  -> WMO id for observation station
geoid -> http://geonames.org id for location
place -> human readable name
         use region to accurate your query (i.e. &place=kumpula,tampere)
         can be used several times in one query (i.e. &place=kumpula,helsinki&place=heinola)
latlon -> coordinates (used only in marine data sets)

